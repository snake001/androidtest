package com.example.myproject;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.myproject.WeChat.AssistantPage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FloatingButtonService extends Service {
    public static boolean isStarted = false;

    private static WindowManager windowManager;
    private static WindowManager.LayoutParams layoutParams;
    Handler mHandler = new Handler();
    private Button button;

    @Override
    public void onCreate() {
        super.onCreate();
        isStarted = true;
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        layoutParams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        }
        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        layoutParams.width = 160;
        layoutParams.height = 160;
        layoutParams.x = 600;
        layoutParams.y = 500;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "float已销毁", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final static int FOREGROUND_ID = 1000;
    private Notification mNotification;
    private static final String TAG_KEY = "KEY_ACCESS";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        showFloatingWindow();
        mHandler.removeMessages(0);
        WechatUtils.isOpenAssistant = false;
        mHandler.postDelayed(checkIsAction, 60000); // 设置为1分钟检测第一次
        if (null != mNotification) {
            Log.i(TAG_KEY, "KeyService->onStartCommand->Notification exists");
            return super.onStartCommand(intent, flags, startId);
        }

        Log.i(TAG_KEY, "KeyService->onStartCommand->Create Notification");
        //NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("鱼乐浮窗服务");
        builder.setContentText("服务ing");
        builder.setContentInfo("Content Info");
        builder.setWhen(System.currentTimeMillis());

        Intent activityIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        mNotification = builder.build();
        startForeground(FOREGROUND_ID, mNotification);
        flags = START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }


    private void showFloatingWindow() {
        if (Settings.canDrawOverlays(this)) {
            button = new Button(getApplicationContext());
            button.setBackgroundResource(R.mipmap.ic_launcher_round);
            windowManager.addView(button, layoutParams);
            button.setOnTouchListener(new FloatingOnTouchListener());
            button.setOnLongClickListener(new onClickView());
        }
    }

    private class onClickView implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View view) {
            SystemManager.onOpenThis();
            return true;
        }
    }

    private class FloatingOnTouchListener implements View.OnTouchListener {
        private int x;
        private int y;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x = (int) event.getRawX();
                    y = (int) event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    int nowX = (int) event.getRawX();
                    int nowY = (int) event.getRawY();
                    int movedX = nowX - x;
                    int movedY = nowY - y;
                    x = nowX;
                    y = nowY;
                    layoutParams.x = layoutParams.x + movedX;
                    layoutParams.y = layoutParams.y + movedY;
                    windowManager.updateViewLayout(view, layoutParams);
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    Runnable checkIsAction = new Runnable() {
        @Override
        public void run() {
            if (MyAccessibilityService.mService != null) {
                Toast.makeText(FloatingButtonService.this, "service运行中", Toast.LENGTH_SHORT).show();
                if (!WechatUtils.isOpenAssistant) { // 服务开了，但是并没有开启
                    if (WechatUtils.isOnMainPage) {
                        EventBus.getDefault().post(new MessageEvent("onCheckAssistant"));
                    } else {
                        mHandler.postDelayed(openMain, 2000);
                        // 假如挂掉，那么120s再执行检测
                        mHandler.postDelayed(this, 120000);
                        return;
                    }
                } else {
                    if (!WechatUtils.isActioning) {
                        WechatUtils.isActioning = true;
                        mHandler.postDelayed(openMain, 2000);
                        // 假如挂掉，那么120s再执行检测
                        mHandler.postDelayed(this, 120000);
                        return;
                    }
                    WechatUtils.isActioning = false;
                }
                //每隔60s循环执行run方法
                mHandler.postDelayed(this, 60000);
            } else {
                mHandler.postDelayed(openMain, 2000);
                // 假如挂掉，那么120s再执行检测
                mHandler.postDelayed(this, 120000);
            }
        }
    };

    Runnable openMain = new Runnable() {
        @Override
        public void run() {
            SystemManager.onOpenThis();
            mHandler.postDelayed(openAssistant, 10000);
        }
    };

    Runnable openAssistant = new Runnable() {
        @Override
        public void run() {
            AssistantPage.onOpenAssistantPage();
        }
    };
}