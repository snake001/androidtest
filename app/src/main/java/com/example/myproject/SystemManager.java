package com.example.myproject;

import android.accessibilityservice.AccessibilityService;
import android.app.Activity;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import java.io.DataOutputStream;
import java.io.OutputStream;

public class SystemManager extends Activity {
    /**
     * 应用程序运行命令获取 Root权限，设备必须已破解(获得ROOT权限)
     *
     * @param command 命令：String apkRoot="chmod 777 "+getPackageCodePath(); RootCommand(apkRoot);
     * @return 应用程序是/否获取Root权限
     */

    /**
     * 7.0.3
     */
    /*private static String backBtnKbId = "kb"; // 返回按钮id
    private static String backBtnKaId = "ka"; // 返回按钮id
    private static String backBtnText = "返回";// 返回*/

    /**
     * 6.7.3
     */
    private static String backBtnKbId = "jc"; // 返回按钮id
    private static String backBtnKaId = "jb"; // 返回按钮id
    private static String backBtnText = "返回";// 返回

    public static boolean RootCommand(String command) {
        Process process = null;
        DataOutputStream os = null;
        try {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            Log.d("*** DEBUG ***", "ROOT REE" + e.getMessage());
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
                Log.d("*** error ***", e.getMessage());
            }
        }
        Log.d("*** DEBUG ***", "Root SUC ");
        return true;
    }

    /**
     * 执行shell命令
     *
     * @param cmd
     */
    public static void execShellCmd(String cmd) {
        try {
            // 申请获取root权限，这一步很重要，不然会没有作用
            Process process = Runtime.getRuntime().exec("su");
            // 获取输出流
            OutputStream outputStream = process.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeBytes(cmd);
            dataOutputStream.flush();
            dataOutputStream.close();
            outputStream.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void delayPageChange() {
        try {
            if (WechatUtils.delayTime > WechatUtils.pageChangeDelayTime) {
                Thread.sleep(WechatUtils.delayTime);
            } else {
                Thread.sleep(WechatUtils.pageChangeDelayTime);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delayLongToOperation() {
        try {
            Thread.sleep(WechatUtils.delayLongTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delayToNextChat() {
        try {
            Thread.sleep(WechatUtils.chatDelayTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delayToOperation() {
        try {
            Thread.sleep(WechatUtils.delayTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delayShortToOperation() {
        try {
            Thread.sleep(WechatUtils.delayShortTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delaySSOperation() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delayScrollOperation() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void onPageBack(AccessibilityService service) {
        SystemManager.delayToOperation();
        AccessibilityNodeInfo nodeBackKa = WechatUtils.findViewById(WechatUtils.getNodeId(backBtnKaId), service);
        if (nodeBackKa != null) {
            WechatUtils.performClick(nodeBackKa);
        } else {
            AccessibilityNodeInfo nodeBackKb = WechatUtils.findViewById(WechatUtils.getNodeId(backBtnKbId), service);
            if (nodeBackKb != null) {
                WechatUtils.performClick(nodeBackKb);
            } else {
                AccessibilityNodeInfo nodeBackText = WechatUtils.findViewByFirstEqualsContentDescription(backBtnText, service);
                if (nodeBackText != null) {
                    WechatUtils.performClick(nodeBackText);
                } else {
                    SystemManager.execShellCmd("input tap 10 70"); // 点击关闭
                }
            }
        }
    }

    public static void onOpenWeChat() {
        SystemManager.execShellCmd("am start com.tencent.mm/com.tencent.mm.ui.LauncherUI"); // 点击关闭
    }

    public static void onOpenThis() {
        SystemManager.execShellCmd("am start com.example.myproject/com.example.myproject.MainActivity"); // 点击关闭
    }
}
