package com.example.myproject;

import android.accessibilityservice.AccessibilityService;
import android.os.Handler;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.example.myproject.WeChat.BiaoQingPage;
import com.example.myproject.WeChat.ChatPage;
import com.example.myproject.WeChat.CreateGroupChatPage;
import com.example.myproject.WeChat.GroupChatInfoPage;
import com.example.myproject.WeChat.GroupChatListPage;
import com.example.myproject.WeChat.SearchPage;
import com.example.myproject.WeChat.SelectContactPage;
import com.example.myproject.WeChat.SelectGroupPage;
import com.example.myproject.WeChat.SendSelectPage;
import com.example.myproject.WeChat.ShareImagePage;
import com.example.myproject.WeChat.ShareSendPage;
import com.example.myproject.WeChat.WeChatMainPage;
import com.example.myproject.WeChat.WebViewPage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class MyAccessibilityService extends AccessibilityService {

    public static MyAccessibilityService mService;

    //微信版本
    private String backBtnText = "返回";// 返回
    private String tongXunText = "通讯录";
    private String moreFunText = "更多功能按钮";
    private String listViewType = "android.widget.ListView";
    private static boolean isChatIng = true;

    Handler mHandler = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            if (WechatUtils.isOpenAssistant) {
                boolean isInChatPage = WechatUtils.isInChatPage ? WechatUtils.isInChatPage : ChatPage.onCheckIsChatWindow(mService);
                if (!isChatIng && isInChatPage) {
                    WechatUtils.isChatNone = false;
                    onChatWindow();
                }
                isChatIng = false;
            }
            //每隔30s循环执行run方法
            mHandler.postDelayed(this, 30000);
        }
    };

    /**
     * 1、点击更多功能，打开群聊
     * 2、选中对象，进入到聊天
     * 3、
     *
     * @param event
     */
    @Override

    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (WechatUtils.isOpenAssistant) { // 有开启服务
            WechatUtils.isActioning = true;
            int eventType = event.getEventType();
            String className = event.getClassName().toString();
            switch (eventType) {
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    /**
                     * 当前界面有变动，并且是聊天界面，然后之前的聊天内容为空的
                     */
                    if (className.equals(listViewType) && WechatUtils.isInChatPage && WechatUtils.isChatNone) {
                        WechatUtils.isChatNone = false;
                        onChatWindow();
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    isChatIng = true;
                    if (WechatUtils.isFirstInActivity) {
                        if (!WechatUtils.isBacking) {
                            onToMainPage();
                        }
                    } else {
                        switch (className) {
                            case WechatUtils.WECHAT_CLASS_LAUNCHUI: // 首页
                                //Toast.makeText(this, "首页", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                onMainPage(true);
                                break;
                            case WechatUtils.WECHAT_CLASS_SEARCH: // 搜索界面
                                //Toast.makeText(this, "搜索界面", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                //onSearchPage();
                                break;
                            case WechatUtils.WECHAT_CLASS_CHATUI: // 微信聊天页面
                                //Toast.makeText(this, "微信聊天页面", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = true;
                                onChatWindow();
                                break;
                            case WechatUtils.WECHAT_CLASS_GROUP_CHAT_INFO_PAGE: // 群聊天信息界面
                                WechatUtils.isInChatPage = false;
                                onGroupChatInfoPage();
                                break;
                            case WechatUtils.WECHAT_CLASS_SELECT_SEND: // 发送选择页面
                                //Toast.makeText(this, "发送选择页面", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                onSendSelectPage();
                                break;
                            case WechatUtils.WECHAT_CLASS_SELECT_PERSON: // 判断是发起群聊还是选择联系人
                                WechatUtils.isInChatPage = false;
                                onCheckGroupChatOrSelectPerson();
                                break;
                            case WechatUtils.WECHAT_CLASS_GROUP_CARD_SELECT: // 选择群聊，勾选群界面
                                //Toast.makeText(this, "选择群聊，勾选群界面", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                onSelectGroupPage(0);
                                break;
                            case WechatUtils.WECHAT_CLASS_WEBVIEW_MP_PAGE:
                            case WechatUtils.WECHAT_CLASS_WEBVIEW_PAGE:
                            case WechatUtils.WECHAT_CLASS_WEBVIEW_LOAD_COMPLETE_PAGE:// webview的窗口
                                //Toast.makeText(this, "webview的窗口", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                onWebViewPage();
                                break;
                            case WechatUtils.WECHAT_CLASS_SHARE_IMAGE_PAGE: // 分享的图片
                                //Toast.makeText(this, "分享的图片", Toast.LENGTH_SHORT).show();
                                WechatUtils.isInChatPage = false;
                                onShareImagePage();
                                break;
                            case WechatUtils.WECHAT_CLASS_BIAO_QING_PAGE: // 表情界面
                                WechatUtils.isInChatPage = false;
                                onBiaoQingPage();
                                break;
                            case WechatUtils.WECHAT_CLASS_CONFIRM_PAGE: // 二级确认弹窗
                                WechatUtils.isInChatPage = false;
                                onDialogConfirm();
                                break;
                            case WechatUtils.WECHAT_CLASS_GROUP_MAIN_PAGE: // 群聊界面
                                onGroupChatListPage();
                                break;
                        }
                    }
                    break;
            }
        }
    }

    private void onToMainPage() {
        WechatUtils.isBacking = true;
        WechatUtils.isFirstInActivity = true;
        SystemManager.delayPageChange();
        AccessibilityNodeInfo nodeBack = WechatUtils.findViewByFirstEqualsContentDescription(backBtnText, this);
        if (nodeBack != null) {
            WechatUtils.performClick(nodeBack);
            onToMainPage();
            return;
        } else { // 判断是否照片
            if (ShareImagePage.isImagePage(mService)) { // 是图片
                ShareImagePage.onClickBack(mService);
                onToMainPage();
                return;
            } else if (ChatPage.isOpenDialog(mService)) {// 打开了聊天dialog
                SystemManager.execShellCmd("input tap 608 300"); // 小米
                onToMainPage();
                return;
            } else if (ShareSendPage.isShareSendPage(mService)) { // 判断是否在确定取消的二级弹窗
                onToMainPage();
                Toast.makeText(MyAccessibilityService.this, "回退", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        WechatUtils.isBacking = false;
        WechatUtils.isFirstInActivity = false;
        onMainPage(false);
    }

    /**
     * 在微信主界面
     */
    private void onMainPage(boolean isBack) {
        SystemManager.delayPageChange();
        AccessibilityNodeInfo moreFunNode = WechatUtils.findViewByFirstContainsContentDescription(moreFunText, mService);
        List<AccessibilityNodeInfo> tongXunNode = WechatUtils.findViewByContainsText(tongXunText, mService);
        if (isBack) {
            if (moreFunNode != null && tongXunNode != null && tongXunNode.size() > 0) {
                onToMainPage();
                return;
            }
        }
        if (moreFunNode != null && tongXunNode != null && tongXunNode.size() > 0) {
            if (TextUtils.isEmpty(WechatUtils.NAME)) return; //如果没有名字，就没有必要搜索了
            if (Utils.isEmptyArray(WechatUtils.allGroupNameList)) {
                if (WeChatMainPage.onClickCommunication(mService)) { // 点击通讯录
                    WeChatMainPage.onClickGroupChatBtn(mService);
                }
                return;
            }

            WeChatMainPage.onClickSearch(this); // 打开搜索界面
            onSearchPage();
        }
    }

    /**
     * 判断是发起群聊还是选择联系人
     */
    private static void onCheckGroupChatOrSelectPerson() {
        SystemManager.delayPageChange();
        if (CreateGroupChatPage.onCheckIsCreateGroupChat(mService)) { // 群聊界面
            //Toast.makeText(this, "发起群聊界面", Toast.LENGTH_SHORT).show();
            onOpenGroupChatPage();
        } else if (SelectContactPage.onCheckIsSelectContact(mService)) {// 选择联系人界面
            //Toast.makeText(this, "选择联系人界面", Toast.LENGTH_SHORT).show();
            onOpenSelectFriend();
        }
    }

    /**
     * 在发起群聊界面
     */
    private static void onOpenGroupChatPage() {
        if (CreateGroupChatPage.onSearchFriend(mService)) { // 搜索成功
            if (CreateGroupChatPage.onClickSearchResult(mService)) { // 成功选中了第一个搜索到的人
                CreateGroupChatPage.onClickConfirm(mService); // 点击确定
            }
        }
    }

    /**
     * 当前正在聊天界面
     */
    private static void onChatWindow() {
        SystemManager.delayPageChange();
        if (ChatPage.onCheckIsSelectGroup(mService)) {
            if (!WechatUtils.isClearChat) { // 第一次进入，未清除聊天记录，则首先清楚聊天记录
                ChatPage.onClickInfo(mService);
                return;
            }
            ChatPage.onScrollTopTop(mService); // 把聊天内容滚动到顶部=》模拟长按（先发一次模拟触摸事件）=》点击发送
            ChatPage.onLongClickMessage(mService);
        }
    }

    /**
     * 群聊的聊天信息界面
     */
    private static void onGroupChatInfoPage() {
        SystemManager.delayPageChange();
        if (!WechatUtils.isClearChat) {
            GroupChatInfoPage.onScrollToBottom(mService); // 界面先滚动到底部
            GroupChatInfoPage.onClickClear(mService);// 点击清除记录
        } else {
            if (!ChatPage.onCheckIsChatWindow(mService)) {
                GroupChatInfoPage.onClickBack(mService);// 点击返回
            }
        }
    }

    /**
     * 当前在发送选择界面
     */
    private static void onSendSelectPage() {
        SystemManager.delayPageChange();
        if (WechatUtils.isSendEnd) {
            SendSelectPage.onClickBack(mService);
            return;
        }
        if (SendSelectPage.onCheckCanSend(mService)) { // 有选中的人
            SendSelectPage.onClickSend(mService);  // 点击确定
        } else { // 无选中的人，点击多选，然后再点击更多联系人，跳转到选择联系人界面
            SendSelectPage.onClickMoreSelect(mService); // 点击多选
            SendSelectPage.onClickMoreFriend(mService); // 点击更多联系人
        }
    }

    /**
     * 当前在选择联系人界面
     */
    private static void onOpenSelectFriend() {
        if (WechatUtils.isSendEnd) {
            SelectContactPage.onClickBack(mService);
            return;
        }
        if (SelectContactPage.onCheckCanSend(mService)) { // 有选中的人，点击确定，跳转到选择
            SelectContactPage.onClickConfirm(mService); // 点击确定
        } else { // 当前没有选中的人，点击群聊
            SelectContactPage.onClickGroupChat(mService); // 选择群聊
        }
    }

    /**
     * 所有发送完毕，回到聊天栏
     */
    private static void onBackToChat() {
        WechatUtils.isSendEnd = true;
        SelectGroupPage.onClickBack(mService);
    }

    /**
     * 选择群聊勾选联系人界面
     */
    private static void onSelectGroupPage(int pageIndex) {
        if (pageIndex == 0) { // 需要滚动到对应的页码
            SystemManager.delayPageChange();
            boolean canSelect = SelectGroupPage.onScrollToPage(WechatUtils.sendLastName, mService); // 滚动到对应的页数
            if (canSelect) { // 可以发送
                onSelectGroupPage(1);
            } else { // 已经发送完了所有群，需要删除对应的聊天记录
                onBackToChat();
            }
        } else {
            List<String> lastListName = SelectGroupPage.onSelectGroup(WechatUtils.sendLastName, WechatUtils.sendOnceCount, mService); // 选中群
            if (WechatUtils.sendLastName != null && WechatUtils.allGroupNameList != null && WechatUtils.sendLastName.size() >= WechatUtils.allGroupNameList.size()) {
                onBackToChat();
                return;
            }
            WechatUtils.onPushListName(lastListName);
            SelectGroupPage.onClickSend(mService);  // 点击确定
        }
    }

    /**
     * 表情界面
     */
    private static void onBiaoQingPage() {
        SystemManager.delayPageChange(); // 等待久一些
        if (!ChatPage.onCheckIsChatWindow(mService)) {
            BiaoQingPage.onClickBack(mService);
        }
    }

    /**
     * webview
     */
    private static void onWebViewPage() {
        SystemManager.delayPageChange(); // 等待久一些
        if (!ChatPage.onCheckIsChatWindow(mService)) {
            SystemManager.delayLongToOperation();
            WebViewPage.onClickRefuse(mService);
            WebViewPage.onClickBack(mService);
        }
    }

    /**
     * 图片界面
     */
    private static void onShareImagePage() {
        SystemManager.delayPageChange();// 图片类，等待久一些
        if (!ChatPage.onCheckIsChatWindow(mService)) {
            if (ShareImagePage.onClickLoadImage(mService)) { // 需要下载
                if (ShareImagePage.onCheckIsLoadComplete(mService)) { // 检测是否下载完毕
                    ShareImagePage.onClickBack(mService); // 一张完毕，然后检查后一张
                }
            } else {
                ShareImagePage.onClickBack(mService);
            }
        }
    }

    private static void onDialogConfirm() {
        SystemManager.delayPageChange();
        String returnString = ShareSendPage.onClickConfirm(mService);
        if (returnString != null) {
            switch (returnString) {
                case ShareSendPage.clearText: //清除
                    WechatUtils.isClearChat = true;
                    onGroupChatInfoPage();
                    Toast.makeText(MyAccessibilityService.mService, "清除", Toast.LENGTH_SHORT).show();
                    break;
                case ShareSendPage.deleteText: //删除
                    WechatUtils.onInitSendOnceEnd();
                    Toast.makeText(MyAccessibilityService.mService, "删除", Toast.LENGTH_SHORT).show();
                    break;
                case ShareSendPage.sendText: //发送
                    if (WechatUtils.sendLastName != null && WechatUtils.allGroupNameList != null && WechatUtils.sendLastName.size() >= WechatUtils.allGroupNameList.size()) {
                        WechatUtils.isSendEnd = true;
                    }
                    break;
            }
            EventBus.getDefault().post(new MessageEvent("save"));
        }
    }

    /**
     * 群聊列表界面
     */
    private void onGroupChatListPage() {
        SystemManager.delayPageChange();
        if (Utils.isEmptyArray(WechatUtils.allGroupNameList)) {
            WechatUtils.allGroupNameList = GroupChatListPage.onGetAllGroup(mService);
            EventBus.getDefault().post(new MessageEvent("save"));
        }
        SystemManager.delayPageChange();
        onToMainPage();
    }


    /**
     * 搜索界面
     */
    private static void onSearchPage() {
        SystemManager.delayPageChange();
        if (SearchPage.onSearchFriend(mService)) { // 搜索成功
            if (SearchPage.onClickSearchResult(mService)) { // 成功选中了第一个搜索到的人
            }
        }
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        mHandler.postDelayed(r, 100);
        mService = this;
        EventBus.getDefault().post(new MessageEvent("serviceConnect"));
    }

    /**
     * 辅助功能是否启动
     */
    public static boolean isStart() {
        return mService != null;
    }

    @Override
    public void onDestroy() {
        mService = null;
        WechatUtils.isOpenAssistant = false;
        EventBus.getDefault().post(new MessageEvent("serviceDestroy"));
        super.onDestroy();
    }
}
