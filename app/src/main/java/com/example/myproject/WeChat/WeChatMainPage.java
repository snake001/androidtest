package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.Utils;
import com.example.myproject.WechatUtils;

import java.util.List;

public class WeChatMainPage {


    /**
     * 7.0.3
     */
    public static String moreFunText = "更多功能按钮"; // 更多功能按钮Text
    public static String groupChatClickText = "发起群聊";
    private static String communicationId = "cw2"; // 通讯录id
    private static String communicationText = "通讯录"; // 更多功能按钮Text
    private static String groupChatText = "群聊"; //

    /**
     * 微信主界面，点击title的更多功能按钮
     */
    public static void onClickMoreFun(AccessibilityService service) {
        AccessibilityNodeInfo searchNode = WechatUtils.findViewByFirstContainsContentDescription(moreFunText, service);
        if (searchNode != null) {
            WechatUtils.performClick(searchNode);
        }
    }

    /**
     * 点击群聊
     */
    public static void onClickGroupChat(AccessibilityService service) {
        // 打开群聊
        List<AccessibilityNodeInfo> list = WechatUtils.findViewByContainsText(groupChatClickText, service);
        if (list != null && list.size() > 0) {
            WechatUtils.performClick(list.get(0));
        }
    }

    /**
     * 点击搜索按钮
     *
     * @param service
     */
    public static void onClickSearch(AccessibilityService service) {
        //SystemManager.execShellCmd("input tap 790 70"); // 谷歌
        SystemManager.execShellCmd("input tap 510 70"); // 小米
    }

    /**
     * 点击通讯录
     *
     * @param service
     * @return
     */
    public static boolean onClickCommunication(AccessibilityService service) {
        List<AccessibilityNodeInfo> list = WechatUtils.findViewByContainsText(communicationText, service);
        if (!Utils.isEmptyArray(list)) {
            WechatUtils.performClick(list.get(0));
            return true;
        }
        return false;
    }

    /**
     * 点击群聊
     *
     * @param service
     */
    public static void onClickGroupChatBtn(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeList = WechatUtils.findViewByContainsText(groupChatText, service);
        if (!Utils.isEmptyArray(nodeList)) {
            String shellCmd = "input tap 50 260";
            SystemManager.execShellCmd(shellCmd); // 一定要有一次触摸事件
        }
    }
}
