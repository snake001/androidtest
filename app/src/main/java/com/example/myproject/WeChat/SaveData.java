package com.example.myproject.WeChat;

import java.io.Serializable;
import java.util.List;

public class SaveData implements Serializable {
    private List<String> sendLastName;
    private List<String> allGroupNameList;
    private boolean isChatNone;
    private boolean isInChatPage;
    private boolean isClearChat;
    private boolean isFirstInActivity;
    private boolean isSendEnd;
    private boolean isImageLoadComplete;
    private boolean isOpenAssistant;
    private String NAME;

    public List<String> getAllGroupList() {
        return allGroupNameList;
    }

    public void setAllGroupList(List<String> list) {
        allGroupNameList = list;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String name) {
        NAME = name;
    }

    public boolean getIsOpenAssistant() {
        return isOpenAssistant;
    }

    public void setIsOpenAssistant(boolean bool) {
        isOpenAssistant = bool;
    }

    public boolean getIsImageLoadComplete() {
        return isImageLoadComplete;
    }

    public void setIsImageLoadComplete(boolean bool) {
        isImageLoadComplete = bool;
    }

    public boolean getIsSendEnd() {
        return isSendEnd;
    }

    public void setIsSendEnd(boolean bool) {
        isSendEnd = bool;
    }

    public boolean getIsFirstInActivity() {
        return isFirstInActivity;
    }

    public void setIsFirstInActivity(boolean bool) {
        isFirstInActivity = bool;
    }

    public boolean getIsClearChat() {
        return isClearChat;
    }

    public void setIsClearChat(boolean bool) {
        isClearChat = bool;
    }

    public boolean getIsInChatPage() {
        return isInChatPage;
    }

    public void setIsInChatPage(boolean bool) {
        isInChatPage = bool;
    }

    public List<String> getSendLastName() {
        return sendLastName;
    }

    public void setSendLastName(List<String> list) {
        sendLastName = list;
    }

    public boolean getIsChatNone() {
        return isChatNone;
    }

    public void setIsChatNone(boolean bool) {
        isChatNone = bool;
    }
}
