package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.List;

public class SearchPage {
    /**
     * 搜索人界面 7.0.3
     */
    /*public static String searchNameText = "搜索";
    public static String searchNameId = "kh";//
    public static String searchListId = "bwh"; //
    public static String searchPersonId = "q0"; //搜索到的人的id
    public static String editTextType = "android.widget.EditText";*/

    /**
     * 搜索人界面 6.7.3
     */
    public static String searchNameText = "搜索";
    public static String searchNameId = "ji";//
    public static String searchListId = "bp0"; //
    public static String searchPersonId = "om"; //搜索到的人的id
    public static String editTextType = "android.widget.EditText";

    /**
     * 搜索对应好友
     */
    public static boolean onSearchFriend(AccessibilityService service) {
        // 复制名称到搜索框;
        SystemManager.delayLongToOperation();
        AccessibilityNodeInfo searchEdiText = WechatUtils.findViewById(WechatUtils.getNodeId(searchNameId), service);
        if (searchEdiText != null && searchEdiText.getClassName().equals(editTextType) && searchEdiText.isEnabled()) {
            searchEdiText.performAction(AccessibilityNodeInfo.FOCUS_INPUT);
            // 先清空内容
            SystemManager.delayShortToOperation();
            WechatUtils.pastContent(service, searchEdiText, "");
            // 复制内容到文本上
            SystemManager.delayShortToOperation();
            WechatUtils.pastContent(service, searchEdiText, WechatUtils.NAME);
            return true;
        }
        return false;
    }

    /**
     * 点击搜索到的结果
     */
    public static Boolean onClickSearchResult(AccessibilityService service) {
        SystemManager.delayShortToOperation();
        AccessibilityNodeInfo nodeInfo = WechatUtils.findViewById(WechatUtils.getNodeId(searchListId), service);
        if (nodeInfo != null && nodeInfo.getChildCount() > 0) {
            for (int i = 0; i < nodeInfo.getChildCount(); i++) {
                List<AccessibilityNodeInfo> nodeChildInfo = nodeInfo.getChild(i).findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(searchPersonId));
                if (nodeChildInfo != null && nodeChildInfo.size() > 0) {
                    WechatUtils.performClick(nodeInfo.getChild(i));
                    return true;
                }
            }
        }
        return false;
    }
}
