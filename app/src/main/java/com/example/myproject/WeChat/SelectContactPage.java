package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.List;

/**
 * 选择联系人界面
 */
public class SelectContactPage {

    /**
     * 7.0.3
     */
    /*public static String selectGroupChatText = "选择群聊";
    public static String selectGroupChatId = "b0d";
    public static String selectGroupId = "dcd"; // 选中的群聊的id
    public static String confirmId = "jx";
    public static String backBtnId = "kb";// 返回按钮id
    public static String selectPersonTitleText = "选择联系人";*/

    /**
     * 6.7.3
     */
    public static String selectGroupChatText = "选择群聊";
    public static String selectGroupChatId = "ave";
    public static String selectGroupId = "d1t"; // 选中的群聊的id
    public static String confirmId = "j0";
    public static String backBtnId = "jc";// 返回按钮id
    public static String selectPersonTitleText = "选择联系人";

    public static boolean onCheckIsSelectContact(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeSelectPersonList = WechatUtils.findViewByContainsText(selectPersonTitleText, service);
        if (nodeSelectPersonList != null && nodeSelectPersonList.size() > 0) {// 选择联系人界面
            return true;
        }
        return false;
    }

    /**
     * 是否可以点击确定发送
     *
     * @return
     */
    public static boolean onCheckCanSend(AccessibilityService service) {
        AccessibilityNodeInfo nodeInfo = WechatUtils.findViewById(WechatUtils.getNodeId(selectGroupId), service);
        if (nodeInfo != null && nodeInfo.getChildCount() > 0) { // 当前有选中的人，点击确定发送
            return true;
        }
        return false;
    }

    public static void onClickConfirm(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(confirmId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
        }
    }

    /**
     * 点击选择群聊
     */
    public static void onClickGroupChat(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeList = WechatUtils.findViewByContainsText(selectGroupChatText, service);
        if (nodeList != null && nodeList.size() > 0) {
            WechatUtils.performClick(nodeList.get(0));
        }
    }

    /**
     * 点击返回
     *
     * @return
     */
    public static void onClickBack(AccessibilityService service) {
        SystemManager.delayPageChange();
        AccessibilityNodeInfo node = WechatUtils.findViewById(WechatUtils.getNodeId(backBtnId), service);
        if (node != null) {
            WechatUtils.performClick(node);
        }
    }
}
