package com.example.myproject.WeChat;

import android.os.Handler;

import com.example.myproject.MessageEvent;
import com.example.myproject.SystemManager;

import org.greenrobot.eventbus.EventBus;

public class AssistantPage {
    private static String appNameText = "鱼乐XiaoMi_6.7.3";
    private static String checkBoxId = "android:id/checkbox";
    private static String setPackage = "com.android.settings";
    private static Handler mHandler = new Handler();

    public static void onOpenAssistantPage() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SystemManager.execShellCmd("input tap 15 60"); // 小米
                SystemManager.delayPageChange();
                SystemManager.execShellCmd("input tap 15 60"); // 小米
                SystemManager.delayPageChange();
                SystemManager.execShellCmd("input tap 250 1200"); // 小米
                AssistantPage.onClickAssistant();
            }
        }, 2000);//3秒后执行Runnable中的run方法
    }

    public static void onClickAssistant() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SystemManager.execShellCmd("input tap 100 250"); // 小米
                AssistantPage.onClickOpenAssistant();
            }
        }, 3000);//3秒后执行Runnable中的run方法
    }

    public static void onClickOpenAssistant() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SystemManager.execShellCmd("input tap 100 180"); // 小米
                AssistantPage.onClickOpenMyApp();
            }
        }, 3000);//3秒后执行Runnable中的run方法
    }

    public static void onClickOpenMyApp() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SystemManager.execShellCmd("input tap 370 1160"); // 小米
                SystemManager.delayPageChange();
                SystemManager.execShellCmd("input tap 15 60"); // 小米
                SystemManager.delayPageChange();
                SystemManager.execShellCmd("input tap 15 60"); // 小米
                SystemManager.delayPageChange();
                SystemManager.execShellCmd("input tap 15 60"); // 小米
                SystemManager.delayPageChange();
                EventBus.getDefault().post(new MessageEvent("openWeChat"));
            }
        }, 3000);//3秒后执行Runnable中的run方法
    }
}
