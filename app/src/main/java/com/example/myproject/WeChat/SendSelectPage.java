package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.List;

/**
 * 选择发送页面
 */
public class SendSelectPage {

    /**
     * 7.0.3
     */
    /*public static String moreSelectId = "jx";
    public static String moreSelectText = "多选";
    public static String moreChatId = "b0d";
    public static String moreChatText = "更多联系人";
    public static String selectGroupId = "dcd"; // 选中的群聊列表的id
    public static String backBtnId = "kb";// 返回按钮id*/

    /**
     * 6.7.3
     */
    public static String moreSelectId = "j0";
    public static String moreSelectText = "多选";
    public static String moreChatId = "ave";
    public static String moreChatText = "更多联系人";
    public static String selectGroupId = "d1t"; // 选中的群聊列表的id
    public static String backBtnId = "jc";// 返回按钮id

    /**
     * 是否可以发送，点击发送
     *
     * @param service
     */
    public static boolean onCheckCanSend(AccessibilityService service) {
        AccessibilityNodeInfo nodeInfo = WechatUtils.findViewById(WechatUtils.getNodeId(selectGroupId), service);
        if (nodeInfo != null && nodeInfo.getChildCount() > 0) { // 当前有选中的人，点击确定发送
            return true;
        }
        return false;
    }

    public static void onClickSend(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(moreSelectId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
        }
    }

    /**
     * 点击多选
     */
    public static void onClickMoreSelect(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeList = WechatUtils.findViewByContainsText(moreSelectText, service);
        if (nodeList != null && nodeList.size() > 0) {
            WechatUtils.performClick(nodeList.get(0));
        }
    }

    /**
     * 点击更多联系人
     */
    public static void onClickMoreFriend(AccessibilityService accessibilityService) {
        SystemManager.delayShortToOperation();
        AccessibilityNodeInfo node = WechatUtils.findViewById(WechatUtils.getNodeId(moreChatId), accessibilityService);
        if (node != null) {
            WechatUtils.performClick(node);
        }
    }

    /**
     * 点击返回
     *
     * @return
     */
    public static void onClickBack(AccessibilityService service) {
        SystemManager.delayPageChange();
        AccessibilityNodeInfo node = WechatUtils.findViewById(WechatUtils.getNodeId(backBtnId), service);
        if (node != null) {
            WechatUtils.performClick(node);
        }
    }
}
