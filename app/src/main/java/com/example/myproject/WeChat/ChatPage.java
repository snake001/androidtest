package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.example.myproject.MessageEvent;
import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * 聊天窗口
 */
public class ChatPage {

    /**
     * 聊天界面 7.0.3
     */
    /*public static String chatBiaoQingId = "aqj"; // 表情的id
    public static String chatUserNameId = "k3";  //  用户名id
    public static String chatTextId = "nu"; // 文本的id
    public static String chatFileId = "aou"; // 分享类型的id、图片类型的id也是这个
    public static String chatUserPicId = "ns"; // 用户的头像的id
    public static String chatListParentId = "alc"; //列表的父层id
    public static String chatImageId = "aqj"; // 图片的id
    public static String chatSendFriendText = "发送给朋友"; // 聊天界面长按弹出发送给好友
    public static String chatDeleteText = "删除"; // 聊天界面长按弹出发送给好友
    public static String backBtnText = "返回";// 返回
    public static String listViewType = "android.widget.ListView";
    public static String dialogChildType = "android.widget.TextView";
    public static String frameLayoutType = "android.widget.FrameLayout";
    public static String infoBtnId = "jy"; // 详细按钮的id*/

    /**
     * 聊天界面 6.7.3
     */
    public static String chatBiaoQingId = "am5"; // 表情的id
    public static String chatUserNameId = "j6";  //  用户名id
    public static String chatImageId = "am5"; // 图片的id
    public static String chatTextId = "mq"; // 文本的id
    public static String chatFileId = "aku"; // 分享类型的id、图片类型的id也是这个
    public static String chatUserPicId = "mo"; // 用户的头像的id
    public static String chatListParentId = "ahf"; //列表的父层id
    public static String infoBtnId = "j1"; // 详细按钮的id
    public static String chatSendFriendText = "发送给朋友"; // 聊天界面长按弹出发送给好友
    public static String chatDeleteText = "删除"; // 聊天界面长按弹出发送给好友
    public static String backBtnText = "返回";// 返回
    public static String listViewType = "android.widget.ListView";
    public static String dialogChildType = "android.widget.TextView";
    public static String frameLayoutType = "android.widget.FrameLayout";


    public static String changeToYYText = "切换到按住说话";
    public static String changeToKeyText = "切换到键盘";
    public static String chatInfoText = "聊天信息";

    public static int touchCount = 0;// 尝试触摸的次数
    public static final String WenBen = "wenben";
    public static final String BiaoQing = "biaoqing";
    public static final String TuPian = "tupian";
    public static final String LianJie = "lianjie";

    public static boolean isOpenDialog(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeSendList = WechatUtils.findViewByContainsText(chatSendFriendText, service);
        if (nodeSendList != null && nodeSendList.size() > 0 && nodeSendList.get(0).getClassName().equals(dialogChildType)) {
            return true;
        }
        return false;
    }

    public static boolean onCheckIsChatWindow(AccessibilityService service) {
        AccessibilityNodeInfo changeToYY = WechatUtils.findViewByFirstContainsContentDescription(changeToYYText, service);
        AccessibilityNodeInfo changeToKeyNode = WechatUtils.findViewByFirstContainsContentDescription(changeToKeyText, service);
        AccessibilityNodeInfo chatInfo = WechatUtils.findViewByFirstContainsContentDescription(chatInfoText, service);
        if ((changeToYY != null || changeToKeyNode != null) && chatInfo != null) {
            return true;
        }
        return false;
    }

    /**
     * 点击聊天信息详细按钮
     */
    public static void onClickInfo(AccessibilityService service) {
        AccessibilityNodeInfo nodeInfo = WechatUtils.findViewById(WechatUtils.getNodeId(infoBtnId), service);
        if (nodeInfo != null) {
            WechatUtils.performClick(nodeInfo);
        }
    }

    public static boolean onCheckIsSelectGroup(AccessibilityService service) {
        if (service != null) {
            String groupName = WechatUtils.findTextById(service, WechatUtils.getNodeId(chatUserNameId));
            if (groupName != null && !groupName.contains(WechatUtils.NAME)) {
                return false;
            }
        }
        return true;
    }

    // 打开聊天窗口
    public static void onScrollTopTop(AccessibilityService service) {
        //如果微信已经处于聊天界面，需要判断当前联系人是不是需要发送的联系人
        String curUserName = WechatUtils.findTextById(service, WechatUtils.getNodeId(chatUserNameId));
        if (!TextUtils.isEmpty(curUserName)) {
            // 成功进入到需要分享内容的聊天界面
            // 1、首先把列表滚到最顶部
            AccessibilityNodeInfo nodeListParent = WechatUtils.findViewById(WechatUtils.getNodeId(chatListParentId), service);
            if (nodeListParent != null && nodeListParent.getChildCount() > 0) {
                AccessibilityNodeInfo nodeList = null;
                for (int i = 0; i < nodeListParent.getChildCount(); i++) {
                    nodeList = nodeListParent.getChild(i);
                    if (nodeList != null && nodeList.getClassName().equals(listViewType)) {
                        break;
                    }
                }
                if (nodeList != null) {
                    int scrollMax = 10;// 假如滚动10次，获取到的都还是没有文字，则默认已经滚动到了顶层
                    int currentScroll = 1;// 当前滚动了多少次累计都是没有文字的
                    int oldTextSize = 0; // 上一个页面的聊天栏文本长度
                    int oldImageSize = 0; // 上一个页面的聊天图片长度
                    int oldFileSize = 0; // 上一个页面的聊天栏链接长度
                    int oldBiaoqingSize = 0; // 上一个页面的聊天栏表情长度
                    boolean chatMessageIsTop = false;
                    while (!chatMessageIsTop) {
                        SystemManager.delayScrollOperation(); // 每次滚动延时
                        List<AccessibilityNodeInfo> imageName = nodeList.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatUserPicId));//获取聊天头像lis
                        List<AccessibilityNodeInfo> textList = nodeList.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatTextId));//获取聊天信息list
                        List<AccessibilityNodeInfo> fileList = nodeList.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatFileId));
                        List<AccessibilityNodeInfo> imageList = nodeList.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatImageId));
                        List<AccessibilityNodeInfo> biaoQingList = nodeList.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatBiaoQingId)); // 表情
                        //有聊天头像说明有聊天记录(但是不一定有文字信息,可能是图片,分享等等)
                        if (imageName != null && imageName.size() != 0) {
                            if (oldTextSize == textList.size() && oldFileSize == fileList.size() && oldImageSize == imageList.size() && oldBiaoqingSize == biaoQingList.size()) {
                                currentScroll++; // 对比上次结果
                            } else {
                                currentScroll = 0;
                            }
                            if (currentScroll >= scrollMax) { // 已经滚动到了顶层了，进行发送操作
                                chatMessageIsTop = true;
                            }
                            oldBiaoqingSize = biaoQingList.size();
                            oldTextSize = textList.size();
                            oldFileSize = fileList.size();
                            oldImageSize = imageList.size();
                            nodeList.performAction(AccessibilityNodeInfo.ACTION_SCROLL_BACKWARD);
                        } else {
                            chatMessageIsTop = true;
                        }
                    }
                    if ((oldTextSize + oldImageSize + oldFileSize + oldBiaoqingSize) <= 0) { // 聊天内容长度为0
                        WechatUtils.isChatNone = true;
                    } else {
                        WechatUtils.isChatNone = false;
                    }
                } else {
                    WechatUtils.isChatNone = true;
                }
            } else {
                WechatUtils.isChatNone = true;
            }
        }
    }

    /**
     * 长按消息
     */
    public static void onLongClickMessage(AccessibilityService service) {
        String curUserName = WechatUtils.findTextById(service, WechatUtils.getNodeId(chatUserNameId));
        if (!TextUtils.isEmpty(curUserName)) {
            SystemManager.delayShortToOperation();
            touchCount = 0;
            AccessibilityNodeInfo nodeListParent = WechatUtils.findViewById(WechatUtils.getNodeId(chatListParentId), service);
            AccessibilityNodeInfo nodeList = null;
            if (nodeListParent != null && nodeListParent.getChildCount() > 0) {
                for (int i = 0; i < nodeListParent.getChildCount(); i++) {
                    if (nodeListParent.getChild(i) != null && nodeListParent.getChild(i).getClassName().equals(listViewType)) {
                        nodeList = nodeListParent.getChild(i);
                        break;
                    }
                }
            }
            AccessibilityNodeInfo nodeSelectItem = null;
            if (nodeList != null && nodeList.getClassName().equals(listViewType)) {
                if (nodeList.getChildCount() > 0) { // 有子项
                    int selectIndex = 0;
                    while (selectIndex < nodeList.getChildCount() && nodeSelectItem == null) { //不断循环下去
                        boolean isBiaoQing = false;
                        AccessibilityNodeInfo nodeCurrent = nodeList.getChild(selectIndex);
                        List<AccessibilityNodeInfo> imageName = nodeCurrent.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatUserPicId));
                        if (imageName != null && imageName.size() > 0) {//获取聊天头像list
                            List<AccessibilityNodeInfo> nodeBQList = nodeCurrent.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatBiaoQingId));
                            if (nodeBQList != null && nodeBQList.size() > 0) { // 如果是表情，直接删除
                                nodeSelectItem = nodeBQList.get(0);
                                if (nodeSelectItem.getClassName().equals(frameLayoutType)) {
                                    isBiaoQing = true;
                                    ChatPage.onLongClickNode(nodeSelectItem, service, true, BiaoQing);
                                }
                            }
                            if (!isBiaoQing) {
                                List<AccessibilityNodeInfo> nodeOZList = nodeCurrent.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatTextId));
                                if (nodeOZList != null && nodeOZList.size() > 0) { // 文本类
                                    nodeSelectItem = nodeOZList.get(0);
                                    ChatPage.onLongClickNode(nodeSelectItem, service, false, WenBen);
                                } else {
                                    List<AccessibilityNodeInfo> nodeARDList = nodeCurrent.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatFileId));
                                    if (nodeARDList != null && nodeARDList.size() > 0) { // 分享类
                                        nodeSelectItem = nodeARDList.get(0);
                                        List<AccessibilityNodeInfo> nodeImage = nodeSelectItem.findAccessibilityNodeInfosByViewId(WechatUtils.getNodeId(chatImageId));
                                        if (nodeImage != null && nodeImage.size() > 0) { // 是图片
                                            if (!WechatUtils.isImageLoadComplete) {
                                                WechatUtils.isTouch = false;
                                            }
                                            ChatPage.onLongClickNode(nodeSelectItem, service, true, TuPian);
                                        } else { // 是链接
                                            ChatPage.onLongClickNode(nodeSelectItem, service, true, LianJie);
                                        }
                                    }
                                }
                            }
                        }
                        selectIndex++;
                    }
                }
            }
        }
    }

    /**
     * 模拟长按聊天
     */
    public static void onLongClickNode(AccessibilityNodeInfo clickNode, AccessibilityService service, boolean isReturn, String textType) {
        if (!WechatUtils.isTouch) {
            WechatUtils.isTouch = true;
            Rect outBounds = new Rect();
            clickNode.getBoundsInScreen(outBounds);
            String shellCmd = "input tap " + (outBounds.left + 20) + " " + (outBounds.top + 10);
            SystemManager.execShellCmd(shellCmd); // 一定要有一次触摸事件
            if (isReturn) {
                return;
            }
            SystemManager.delayLongToOperation(); // 短延迟，打开dialog
        }
        WechatUtils.performLongClick(clickNode); //
        SystemManager.delayLongToOperation(); // 操作延迟

        if (WechatUtils.isSendEnd || textType == BiaoQing) { // 当前信息已发送完毕，需要删除
            List<AccessibilityNodeInfo> nodeSendList = WechatUtils.findViewByContainsText(chatDeleteText, service);
            if (nodeSendList != null && nodeSendList.size() > 0) {
                WechatUtils.performClick(nodeSendList.get(0)); // 点击删除该条信息
                WechatUtils.onInitSendOnceEnd();
                EventBus.getDefault().post(new MessageEvent("save"));
                return;
            }
        } else {
            List<AccessibilityNodeInfo> nodeSendList = WechatUtils.findViewByContainsText(chatSendFriendText, service);
            if (nodeSendList != null && nodeSendList.size() > 0 && nodeSendList.get(0).getClassName().equals(dialogChildType)) {
                WechatUtils.performClick(nodeSendList.get(0)); // 点击更多
                return;
            }
        }
        touchCount++;
        if (touchCount <= 5) {
            WechatUtils.isTouch = false;
            onLongClickNode(clickNode, service, isReturn, textType);
            return;
        }

        // 失败了
        Toast.makeText(service, "失败了，root权限没有开", Toast.LENGTH_SHORT).show();
    }
}
