package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.List;

public class WebViewPage {

    private static String refuseText = "拒绝";

    public static void onClickBack(AccessibilityService service) {
        SystemManager.onPageBack(service);
    }

    public static void onClickRefuse(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeInfo = WechatUtils.findViewByContainsText(refuseText, service);
        if (nodeInfo != null && nodeInfo.size() > 0) {
            WechatUtils.performClick(nodeInfo.get(0));
            SystemManager.delayPageChange();
        }
    }
}
