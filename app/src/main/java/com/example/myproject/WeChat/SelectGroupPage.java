package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.ArrayList;
import java.util.List;

public class SelectGroupPage {
    /**
     * 7.0.3
     */
    /*public static String groupNameId = "c_4"; // 群名称的id
    public static String groupListId = "c_1"; // 列表的id
    public static String confirmId = "jx";
    public static String backBtnId = "kb";// 返回按钮id*/

    /**
     * 6.7.3
     */
    public static String groupNameId = "c2n"; // 群名称的id
    public static String groupListId = "c2k"; // 列表的id
    public static String confirmId = "j0";
    public static String backBtnId = "jc";// 返回按钮id

    public static List<String> newListName = new ArrayList<>();
    public static List<String> currentList = new ArrayList<>();
    public static int selectCount = 0;


    public static void onSelect(List<String> oldListName, int selectMax, AccessibilityService service) {
        SystemManager.delayShortToOperation();
        AccessibilityNodeInfo nodeList = WechatUtils.findViewById(WechatUtils.getNodeId(groupListId), service);
        currentList = new ArrayList<>();
        for (int i = 0; i < nodeList.getChildCount(); i++) {
            String groupName = WechatUtils.findTextById(nodeList.getChild(i), WechatUtils.getNodeId(groupNameId));
            currentList.add(groupName);
            if ((!WechatUtils.checkIsInList(oldListName, groupName) || oldListName.size() <= 0)
                    && selectCount < selectMax && !WechatUtils.checkIsInList(newListName, groupName)) {
                if (!groupName.equals(WechatUtils.NAME)) {
                    selectCount++;
                    WechatUtils.performClick(nodeList.getChild(i));
                }
                newListName.add(groupName);
                SystemManager.delaySSOperation(); // 每等待0.1秒，才选中一个人
            }
        }
    }

    public static List<String> onSelectGroup(List<String> oldListName, int selectMax, AccessibilityService service) {
        newListName = new ArrayList<>();
        selectCount = 0;
        AccessibilityNodeInfo nodeList = WechatUtils.findViewById(WechatUtils.getNodeId(groupListId), service);
        if (nodeList != null) {
            onSelect(oldListName, selectMax, service);
            if (selectCount < selectMax) { // 假如当前页面选择不够9个，则滚动一页，继续选择
                WechatUtils.performScrollForWard(nodeList);
                onSelect(oldListName, selectMax, service);
            }
        }
        return newListName;
    }

    /**
     * 滚动到对应的页数
     *
     * @param lastName
     * @param service
     * @return
     */
    public static boolean onScrollToPage(List<String> lastName, AccessibilityService service) {
        boolean isCanSelect = false;
        AccessibilityNodeInfo nodeList = WechatUtils.findViewById(WechatUtils.getNodeId(groupListId), service);
        if (nodeList != null) {
            boolean isEnd = false;
            List<String> nameList = new ArrayList<>();
            while (!isEnd) {
                // 判断是否到底发送完毕
                List<String> newtList = new ArrayList<>();
                for (int i = 0; i < nodeList.getChildCount(); i++) {
                    String groupName = WechatUtils.findTextById(nodeList.getChild(i), WechatUtils.getNodeId(groupNameId));
                    if (!TextUtils.isEmpty(groupName) && !groupName.equals(WechatUtils.NAME)) {
                        if (!WechatUtils.checkIsInList(lastName, groupName)) {
                            isEnd = true;
                            isCanSelect = true;
                            break;
                        }
                        if (!WechatUtils.checkIsInList(newtList, groupName)) {
                            nameList.add(groupName);
                        }
                    }
                }
                if (!isCanSelect) {
                    if (nameList.size() >= WechatUtils.allGroupNameList.size()) {
                        isEnd = true;
                    }
                    WechatUtils.performScrollForWard(nodeList);
                    SystemManager.delayScrollOperation();
                }
            }
        }
        return isCanSelect;
    }

    /**
     * 点击确定发送
     *
     * @param service
     * @return
     */
    public static boolean onClickSend(AccessibilityService service) {
        SystemManager.delayShortToOperation();
        AccessibilityNodeInfo node = WechatUtils.findViewById(WechatUtils.getNodeId(confirmId), service);
        if (node != null) {
            WechatUtils.performClick(node);
            return true;
        }
        return false;
    }

    /**
     * 点击返回
     *
     * @return
     */
    public static void onClickBack(AccessibilityService service) {
        SystemManager.delayPageChange();
        AccessibilityNodeInfo node = WechatUtils.findViewById(WechatUtils.getNodeId(backBtnId), service);
        if (node != null) {
            WechatUtils.performClick(node);
        }
    }
}
