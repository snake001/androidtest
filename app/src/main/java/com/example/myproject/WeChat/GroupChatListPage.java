package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.ArrayList;
import java.util.List;

public class GroupChatListPage {

    private static String groupListId = "li"; // 列表id
    private static String groupNameId = "m6"; // 群组的名称id

    public static List<String> onGetAllGroup(AccessibilityService service) {
        int AddCount = 1;
        List<String> nameList = new ArrayList<>();
        while (AddCount > 0) {
            AddCount = 0;
            AccessibilityNodeInfo nodeList = WechatUtils.findViewById(WechatUtils.getNodeId(groupListId), service);
            if (nodeList != null && nodeList.getChildCount() > 0) {
                for (int i = 0; i < nodeList.getChildCount(); i++) {
                    String groupName = WechatUtils.findTextById(nodeList.getChild(i), WechatUtils.getNodeId(groupNameId));
                    if (!TextUtils.isEmpty(groupName)) {
                        if (!WechatUtils.checkIsInList(nameList, groupName) && !groupName.equals(WechatUtils.NAME)) {
                            AddCount++;
                            nameList.add(groupName);
                        }
                    }
                }
            }
            SystemManager.delayShortToOperation();
            WechatUtils.performScrollForWard(nodeList);
            SystemManager.delayPageChange();
        }
        return nameList;
    }
}
