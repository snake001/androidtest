package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

import java.util.List;

public class ChatInfoPage {
    /**
     * 7.0.3
     */
    /*public static String listParentId = "d9d"; // 列表的id
    public static String clearText = "清空聊天记录"; // 清空聊天记录

    /**
     * 6.7.3
     */
    public static String listParentId = "cxx"; // 列表的id
    public static String clearText = "清空聊天记录"; // 清空聊天记录

    public static int scrollCount = 3;

    public static void onScrollToBottom(AccessibilityService service) {
        AccessibilityNodeInfo nodeList = WechatUtils.findViewById(WechatUtils.getNodeId(listParentId), service);
        if (nodeList != null && nodeList.getChild(0) != null) {
            // 列表先滚动几次
            for (int i = 0; i < scrollCount; i++) {
                WechatUtils.performScrollForWard(nodeList.getChild(0));
                SystemManager.delayScrollOperation();
            }
        }
    }

    /**
     * 点击清空记录
     *
     * @param service
     * @return
     */
    public static void onClickClear(AccessibilityService service) {
        SystemManager.delayToOperation();
        List<AccessibilityNodeInfo> list = WechatUtils.findViewByContainsText(clearText, service);
        if (list != null && list.size() > 0) {
            WechatUtils.performClick(list.get(0));
        }
    }

    /**
     * 点击删除
     */
    public static void onClickDelete(AccessibilityService service) {
        ShareSendPage.onClickClear(service);
    }

    /**
     * 点击返回
     *
     * @param service
     */
    public static void onClickBack(AccessibilityService service) {
        SystemManager.onPageBack(service);
    }
}
