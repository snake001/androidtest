package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.SystemManager;
import com.example.myproject.WechatUtils;

public class ShareImagePage {

    /**
     * 7.0.3
     */
    /*public static String checkImageId = "b6k"; // 查看原图按钮
    public static String loadImagePercentId = "cg7"; // 图片下载百分比id*/

    /**
     * 6.7.3
     */
    public static String checkImageId = "b0h"; // 查看原图按钮
    public static String loadImagePercentId = "cgi"; // 图片下载百分比id
    public static String ImageId = "aa9"; // 图片id

    /**
     * 是否图片
     *
     * @param service
     * @return
     */
    public static boolean isImagePage(AccessibilityService service) {
        AccessibilityNodeInfo nodeImage = WechatUtils.findViewById(WechatUtils.getNodeId(ImageId), service);
        if (nodeImage != null) { //
            return true;
        }
        return false;
    }

    /**
     * 点击下载原图
     */
    public static boolean onClickLoadImage(AccessibilityService service) {
        AccessibilityNodeInfo nodeCheck = WechatUtils.findViewById(WechatUtils.getNodeId(checkImageId), service);
        if (nodeCheck != null) { // 有查看原图按钮，点击查看原图
            WechatUtils.performClick(nodeCheck);
            return true;
        }
        return false;
    }

    public static void onClickBack(AccessibilityService service) {
        WechatUtils.isImageLoadComplete = true;
        SystemManager.execShellCmd("input tap 400 550"); // 小米
        //SystemManager.execShellCmd("input tap 600 850"); // 谷歌
    }

    /**
     * 检测是否正在下载中
     *
     * @param service
     */
    public static boolean onCheckIsLoadComplete(AccessibilityService service) {
        boolean isComplete = false;
        while (!isComplete) {
            SystemManager.delayShortToOperation();
            AccessibilityNodeInfo nodeCheck = WechatUtils.findViewById(WechatUtils.getNodeId(loadImagePercentId), service);
            if (nodeCheck != null) { // 有下载的按钮，正在下载中
            } else { // 已经下载完毕
                isComplete = true;
                onClickLoadImage(service);
            }
        }
        return true;
    }
}
