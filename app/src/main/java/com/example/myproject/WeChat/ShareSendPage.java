package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.WechatUtils;

/**
 * 分享发送弹窗
 */
public class ShareSendPage {

    /**
     * 7.0.3
     */
    /*public static String shareSendId = "az_";// 发送的按钮的id*/

    /**
     * 6.7.3
     */
    public static String shareSendId = "au_";// 发送的按钮的id
    public static String cancleId = "au9";// 取消的按钮的id
    public static final String deleteText = "删除";// 删除的
    public static final String sendText = "发送"; // 发送的
    public static final String clearText = "清空"; // 发送的
    public static final String cancleText = "取消"; // 发送的


    public static boolean isShareSendPage(AccessibilityService service) {
        AccessibilityNodeInfo nodeCancle = WechatUtils.findViewById(WechatUtils.getNodeId(cancleId), service);
        if (nodeCancle != null) {
            if (nodeCancle.getText().equals(cancleText)) {
                WechatUtils.performClick(nodeCancle);
                return true;
            }
        }
        return false;
    }

    public static String onClickConfirm(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(shareSendId), service);
        if (nodeConfirm != null) {
            if (nodeConfirm.getText().equals(deleteText)) {
                if (onClickDelete(service)) {
                    return deleteText;
                }
            } else if (nodeConfirm.getText().equals(sendText)) {
                if (onClickSend(service)) {
                    return sendText;
                }
            } else if (nodeConfirm.getText().equals(clearText)) {
                if (onClickClear(service)) {
                    return clearText;
                }
            }
        }
        return null;
    }


    /**
     * 6.7.3
     */
    /*public static String shareSendId = "au_";// 发送的按钮的id*/
    public static boolean onClickSend(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(shareSendId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
            return true;
        }
        return false;
    }

    public static boolean onClickClear(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(shareSendId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
            return true;
        }
        return false;
    }

    /**
     * 删除
     *
     * @param service
     * @return
     */
    public static boolean onClickDelete(AccessibilityService service) {
        // 点击删除按钮
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(shareSendId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
            return true;
        }
        return false;
    }
}
