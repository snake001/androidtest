package com.example.myproject.WeChat;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.myproject.WechatUtils;

import java.util.List;

/**
 * 发起群聊界面
 */
public class CreateGroupChatPage {

    /**
     * 7.0.3
     */
    /*public static String groupChatSearchId = "b8a"; // 搜索文本的id
    public static String groupChatSearchPersonId = "dat"; //搜索到的人的id
    public static String groupChatConfirmId = "jx"; // 确定按钮*/

    /**
     * 6.7.3
     */
    public static String groupChatSearchId = "b26"; // 搜索文本的id
    public static String groupChatSearchPersonId = "d09"; //搜索到的人的id
    public static String groupChatConfirmId = "j0"; // 确定按钮

    public static String editTextType = "android.widget.EditText";
    private static String groupChatTitleText = "发起群聊";

    public static boolean onCheckIsCreateGroupChat(AccessibilityService service) {
        List<AccessibilityNodeInfo> nodeGroupChatList = WechatUtils.findViewByContainsText(groupChatTitleText, service);
        if (nodeGroupChatList != null && nodeGroupChatList.size() > 0) { // 群聊界面
            return true;
        }
        return false;
    }

    /**
     * 搜索对应好友
     */
    public static boolean onSearchFriend(AccessibilityService service) {
        // 复制名称到搜索框;
        AccessibilityNodeInfo searchEdiText = WechatUtils.findViewById(WechatUtils.getNodeId(groupChatSearchId), service);
        if (searchEdiText != null && searchEdiText.getClassName().equals(editTextType) && searchEdiText.isEnabled()) {
            try {
                searchEdiText.performAction(AccessibilityNodeInfo.FOCUS_INPUT);
                // 先清空内容
                Thread.sleep(500);
                WechatUtils.pastContent(service, searchEdiText, "");
                // 复制内容到文本上
                Thread.sleep(500);
                WechatUtils.pastContent(service, searchEdiText, WechatUtils.NAME);
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }


    /**
     * 点击搜索到的结果
     */
    public static Boolean onClickSearchResult(AccessibilityService service) {
        AccessibilityNodeInfo nodeInfo = WechatUtils.findViewById(WechatUtils.getNodeId(groupChatSearchPersonId), service);
        if (nodeInfo != null) {
            WechatUtils.performClick(nodeInfo);
            return true;
        }
        return false;
    }

    /**
     * 点击确定
     *
     * @param service
     */
    public static void onClickConfirm(AccessibilityService service) {
        AccessibilityNodeInfo nodeConfirm = WechatUtils.findViewById(WechatUtils.getNodeId(groupChatConfirmId), service);
        if (nodeConfirm != null) {
            WechatUtils.performClick(nodeConfirm);
        }
    }
}
