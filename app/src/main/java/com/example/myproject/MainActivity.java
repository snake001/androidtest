package com.example.myproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myproject.WeChat.SaveData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText textName; // 搜索的名称
    private EditText sendTime; // 每次发送间隔
    private EditText operationTime; // 操作间隔
    private EditText onceShowCount; // 一屏幕显示多少个群
    private EditText groupCount; // 总共多少个群
    private EditText onceSendCount; // 一次发送数量
    private Switch switchUse; // 是否开启
    private boolean isGetData = true;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static SaveData _saveData;
    private int delayOpenWeChat = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SystemManager.execShellCmd("input tap 100 300");
        startFloatingButtonService();
        isGetData = true;
        onShowPage();
        onCheckService();
        //注册事件
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //注销事件
        EventBus.getDefault().unregister(this);
    }

    private void onCheckService() {
        if (isGetData) {
            WechatUtils.isOnMainPage = true;
            isGetData = false;
            onGetName();
            getLocalData();
            if (WechatUtils.NAME != null) {
                textName.setText(WechatUtils.NAME);
            }
        }
        WechatUtils.isOpenAssistant = false;
        switchUse.setChecked(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onCheckService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        WechatUtils.isOnMainPage = false;
        isGetData = true;
    }

    private void onShowPage() {
        textName = (EditText) findViewById(R.id.textName);
        sendTime = (EditText) findViewById(R.id.textSendTime);
        operationTime = (EditText) findViewById(R.id.textOperationTime);
        onceShowCount = (EditText) findViewById(R.id.textOnceShowGroup);
        groupCount = (EditText) findViewById(R.id.textGroupCount);
        onceSendCount = (EditText) findViewById(R.id.textSendCount);
        switchUse = (Switch) findViewById(R.id.switchUse);

        Button btnClear = (Button) findViewById(R.id.btnClear);
        Button btnOpen = (Button) findViewById(R.id.btnOpenSwitch);
        switchUse.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnOpen.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClear: // 初始化
                onClickClear();
                break;
            case R.id.btnOpenSwitch: // 打开辅助开关操作
                onClickOpenAssistant();
                break;
            case R.id.switchUse: // 服务开启关闭点击
                onClickSwitch();
                break;
        }
    }

    /**
     * 点击初始化
     */
    private void onClickClear() {
        sendTime.setText("1");
        operationTime.setText("1");
        onceShowCount.setText("10");
        groupCount.setText("100");
        onceSendCount.setText("9");
        switchUse.setChecked(false);

        onGetTextValue();
        WechatUtils.onInitData();
        setLocalData();
    }

    private void onGetTextValue() {
        WechatUtils.NAME = textName.getText() != null ? textName.getText().toString() : "";
        WechatUtils.sendOnceCount = Integer.parseInt(onceSendCount.getText() != null ? onceSendCount.getText().toString() : "0");
        WechatUtils.onePageShowCount = Integer.parseInt(onceShowCount.getText() != null ? onceShowCount.getText().toString() : "0");
        WechatUtils.maxGroupCount = Integer.parseInt(groupCount.getText() != null ? groupCount.getText().toString() : "0");
        WechatUtils.chatDelayTime = Integer.parseInt(sendTime.getText() != null ? sendTime.getText().toString() : "0") * 1000;
        WechatUtils.delayTime = Integer.parseInt(operationTime.getText() != null ? operationTime.getText().toString() : "0") * 1000;
        WechatUtils.isOpenAssistant = switchUse.isChecked();
    }

    /**
     * 打开辅助开关
     */
    private void onClickOpenAssistant() {
        try {
            this.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        } catch (Exception e) {
            this.startActivity(new Intent(Settings.ACTION_SETTINGS));
            e.printStackTrace();
        }
    }

    /**
     * 点击开关
     */
    private void onClickSwitch() {
        WechatUtils.isBacking = false;
        WechatUtils.isFirstInActivity = true;
        WechatUtils.isInChatPage = false;
        WechatUtils.isOpenAssistant = switchUse.isChecked();
        if (WechatUtils.isOpenAssistant) { // 当前开关开
            boolean isError = false;
            onGetTextValue();
            if (WechatUtils.NAME == null || WechatUtils.NAME.length() <= 0 || WechatUtils.NAME == "") {
                isError = true;
                Toast.makeText(this, "请填写要收集的好友", Toast.LENGTH_SHORT).show();
            } else if (WechatUtils.sendOnceCount < 1 || WechatUtils.sendOnceCount > 9) {
                isError = true;
                Toast.makeText(this, "一次可发送群：1~9", Toast.LENGTH_SHORT).show();
            } else if (WechatUtils.onePageShowCount < 1) {
                isError = true;
                Toast.makeText(this, "请填写一屏幕可现实多少个群列表", Toast.LENGTH_SHORT).show();
            } else if (WechatUtils.maxGroupCount < 1) {
                isError = true;
                Toast.makeText(this, "请填写共有多少个群", Toast.LENGTH_SHORT).show();
            } else if (WechatUtils.chatDelayTime < 1000) {
                isError = true;
                Toast.makeText(this, "每次发送间隔不能低于1秒", Toast.LENGTH_SHORT).show();
            } else if (WechatUtils.delayTime < 1000) {
                isError = true;
                Toast.makeText(this, "每次操作间隔不能低于1秒", Toast.LENGTH_SHORT).show();
            }
            if (isError) {
                switchUse.setChecked(false);
                WechatUtils.isOpenAssistant = switchUse.isChecked();
            } else {
                saveName();
                WechatUtils.isOnMainPage = false;
                SystemManager.onOpenWeChat();
            }
        }
    }

    /**
     * 开启浮窗
     */
    public void startFloatingButtonService() {
        if (FloatingButtonService.isStarted) {
            return;
        }
        if (!Settings.canDrawOverlays(this)) {
            Toast.makeText(this, "当前无权限，请授权", Toast.LENGTH_SHORT);
            startActivityForResult(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName())), 0);
        } else {
            startService(new Intent(MainActivity.this, FloatingButtonService.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "授权失败", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "授权成功", Toast.LENGTH_SHORT).show();
                startService(new Intent(MainActivity.this, FloatingButtonService.class));
            }
        }
    }

    /**
     * 保存名称
     */
    public void saveName() {
        if (preferences == null) {
            preferences = getSharedPreferences("userName", Context.MODE_PRIVATE);
        }
        if (editor == null) {
            editor = preferences.edit();
        }
        editor.putString("NAME", WechatUtils.NAME);
        editor.commit();
    }

    /**
     * 获取名称
     */
    public void onGetName() {
        if (preferences == null) {
            preferences = getSharedPreferences("userName", Context.MODE_PRIVATE);
        }
        WechatUtils.NAME = preferences.getString("NAME", WechatUtils.NAME);
    }

    /**
     * 设置缓存信息
     */
    public void setLocalData() {
        _saveData = new SaveData();
        _saveData.setIsChatNone(WechatUtils.isChatNone);
        _saveData.setIsClearChat(WechatUtils.isClearChat);
        _saveData.setIsFirstInActivity(true);
        _saveData.setIsInChatPage(WechatUtils.isInChatPage);
        _saveData.setIsOpenAssistant(WechatUtils.isOpenAssistant);
        _saveData.setSendLastName(WechatUtils.sendLastName);
        _saveData.setIsImageLoadComplete(WechatUtils.isImageLoadComplete);
        _saveData.setIsSendEnd(WechatUtils.isSendEnd);
        _saveData.setAllGroupList(WechatUtils.allGroupNameList);

        File path = this.getApplicationContext().getFilesDir();   //获取存储的路径
        File file = new File(path + "/WeChatData.txt");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(_saveData);
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取缓存信息
     */
    public void getLocalData() {
        File path = this.getApplicationContext().getFilesDir();
        File file = new File(path + "/WeChatData.txt");
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(path + "/WeChatData.txt");
                ObjectInputStream ois = new ObjectInputStream(fis);
                SaveData saveData = (SaveData) ois.readObject();

                WechatUtils.isChatNone = saveData.getIsChatNone();
                WechatUtils.isFirstInActivity = saveData.getIsFirstInActivity();
                WechatUtils.isInChatPage = saveData.getIsInChatPage();
                WechatUtils.isOpenAssistant = saveData.getIsOpenAssistant();
                WechatUtils.sendLastName = saveData.getSendLastName();
                WechatUtils.isImageLoadComplete = saveData.getIsImageLoadComplete();
                WechatUtils.isSendEnd = saveData.getIsSendEnd();
                WechatUtils.isClearChat = saveData.getIsClearChat();
                WechatUtils.allGroupNameList = saveData.getAllGroupList();
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 侦听EventBus
     *
     * @param messageEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowEventMessage(MessageEvent messageEvent) {
        if (messageEvent.getMessage().equals("save")) {  // 保存
            setLocalData();
        } else if (messageEvent.getMessage().equals("openWeChat")) { // 打开微信
            Toast.makeText(this, "assistant重启", Toast.LENGTH_SHORT).show();
            mHandler.removeMessages(0);
            mHandler.postDelayed(openWeChat, delayOpenWeChat);
        } else if (messageEvent.getMessage().equals("onCheckAssistant")) {  // 检测是否有打开辅助
            if (WechatUtils.isOnMainPage) {
                mHandler.removeMessages(0);
                mHandler.postDelayed(openWeChat, delayOpenWeChat);
            }
        }
    }

    /**
     * 打开微信
     */
    Handler mHandler = new Handler();
    Runnable openWeChat = new Runnable() {
        @Override
        public void run() {
            switchUse.setChecked(true);
            onClickSwitch();
        }
    };
}
