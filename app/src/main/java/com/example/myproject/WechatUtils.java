package com.example.myproject;

import android.accessibilityservice.AccessibilityService;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class WechatUtils {
    public static final String WECHAT_CLASS_LAUNCHUI = "com.tencent.mm.ui.LauncherUI";//微信首页
    public static final String WECHAT_CLASS_CHATUI = "com.tencent.mm.ui.chatting.ChattingUI";//微信聊天页面
    public static final String WECHAT_CLASS_SEARCH = "com.tencent.mm.plugin.fts.ui.FTSMainUI";//微信搜索页面
    public static final String WECHAT_CLASS_SELECT_SEND = "com.tencent.mm.ui.transmit.SelectConversationUI";//微信发送选择页面
    public static final String WECHAT_CLASS_SELECT_PERSON = "com.tencent.mm.ui.contact.SelectContactUI";//微信选择联系人页面
    public static final String WECHAT_CLASS_GROUP_CARD_SELECT = "com.tencent.mm.ui.contact.GroupCardSelectUI"; //微信群聊
    public static final String WECHAT_CLASS_GROUP_MAIN_PAGE = "com.tencent.mm.ui.contact.ChatroomContactUI"; // 群聊主界面
    public static final String WECHAT_CLASS_WEBVIEW_MP_PAGE = "com.tencent.mm.plugin.webview.ui.tools.WebviewMpUI"; // 分享的webview
    public static final String WECHAT_CLASS_WEBVIEW_PAGE = "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"; // 分享的webview
    public static final String WECHAT_CLASS_WEBVIEW_LOAD_COMPLETE_PAGE = "com.tencent.mm.plugin.webview.ui.tools.preload.TmplWebViewTooLMpUI"; // 分享的webview加载完
    public static final String WECHAT_CLASS_SHARE_IMAGE_PAGE = "com.tencent.mm.ui.chatting.gallery.ImageGalleryUI"; // 分享的图片
    public static final String WECHAT_CLASS_CHAT_INFO_PAGE = "com.tencent.mm.ui.SingleChatInfoUI"; // 聊天信息界面
    public static final String WECHAT_CLASS_GROUP_CHAT_INFO_PAGE = "com.tencent.mm.chatroom.ui.ChatroomInfoUI"; // 聊天信息界面
    public static final String WECHAT_CLASS_BIAO_QING_PAGE = "com.tencent.mm.plugin.emoji.ui.CustomSmileyPreviewUI"; // 表情界面
    public static final String WECHAT_CLASS_CONFIRM_PAGE = "com.tencent.mm.ui.widget.a.c"; // 确认窗口


    public static String NAME;
    public static String CONTENT;
    public static boolean isOpenAssistant = false; // 辅助是否开启
    public static int sendOnceCount = 9; // 单次可以发送9个群
    public static int onePageShowCount = 10; // 一个屏幕，显示多少个群
    public static int delayTime = 1000; // 每个操作的间隔
    public static int delayShortTime = 500; // 短的间隔
    public static int chatDelayTime = 1000; // 每次发送的间隔
    public static int maxGroupCount = 100; // 当前账号，最大群数量
    public static int pageChangeDelayTime = 2000; // 界面切换等候时间
    public static int delayLongTime = 2000; // 长等候时间

    /**
     * 界面变动会修改
     */
    public static boolean isChatNone = false; //是否聊天内容为空
    public static boolean isInChatPage = false; // 当前是否正在聊天界面中
    public static boolean isOnMainPage = false; // 是否在主界面

    /**
     * 程序启动初始化
     */
    public static boolean isClearChat = false; // 当前是否已经清理过聊天记录
    public static boolean isTouch = false;
    public static boolean isFirstInActivity = true;// 是否第一次启动

    /**
     * 一条信息发送完初始化
     */
    public static boolean isImageLoadComplete = false; // 当前图片是否已经加载原图完毕
    public static List<String> sendLastName = new ArrayList<>(); // 上次发送的最后一个人的名称
    public static boolean isSendEnd = false; // 当前信息已经全部发送群完毕

    public static boolean isBacking = false;
    public static List<String> allGroupNameList = new ArrayList<>(); // 所有群昵称
    public static boolean isActioning = true; // 是否正在动作？

    /**
     * 数据初始化
     */
    public static void onInitData() {
        isChatNone = false; //是否聊天内容为空
        isInChatPage = false; // 当前是否正在聊天界面中

        isClearChat = false; // 当前是否已经清理过聊天记录
        isTouch = false;
        isFirstInActivity = true;// 是否第一次启动

        isImageLoadComplete = false; // 当前图片是否已经加载原图完毕
        sendLastName = new ArrayList<>(); // 上次发送的最后一个人的名称
        allGroupNameList = new ArrayList<>(); // 所有群昵称
        isSendEnd = false; // 当前信息已经全部发送群完毕

        isOpenAssistant = false;
        isBacking = false;
    }

    /**
     * 一条信息发送完毕初始化
     */
    public static void onInitSendOnceEnd() {
        isImageLoadComplete = false;
        sendLastName = new ArrayList<>();
        isSendEnd = false;
    }

    /**
     * 把上次的发送的列表加入到已发送过的列表内
     *
     * @param listName
     */
    public static void onPushListName(List<String> listName) {
        if (sendLastName != null && listName != null) {
            for (int i = 0; i < listName.size(); i++) {
                sendLastName.add(listName.get(i));
            }
        }
    }

    /**
     * 列表1，是否在列表2内存在
     *
     * @param listFirst
     * @param listSecond
     * @return
     */
    public static boolean isInOtherList(List<String> listFirst, List<String> listSecond) {
        if (listFirst != null && listSecond != null && listSecond.size() > 0 && listSecond.size() >= listFirst.size()) {
            for (int i = 0; i < listFirst.size(); i++) {
                boolean isHave = false;
                if (listFirst.get(i) != null) {
                    for (int j = 0; j < listSecond.size(); j++) {
                        if (listFirst.get(i).equals(listSecond.get(j))) {
                            isHave = true;
                            break;
                        }
                    }
                    if (!isHave) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 如果已经在列表内，则返回true
     *
     * @param listName
     * @param currentName
     * @return
     */
    public static boolean checkIsInList(List<String> listName, String currentName) {
        if (listName != null && listName.size() > 0) {
            for (int i = 0; i < listName.size(); i++) {
                if (listName.get(i).equals(currentName)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * 在当前页面查找文字内容并点击
     *
     * @param text
     */
    public static void findTextAndClick(AccessibilityService accessibilityService, String text) {
        if (accessibilityService == null) {
            return;
        }
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityService.getRootInActiveWindow();
        if (accessibilityNodeInfo == null) {
            return;
        }

        List<AccessibilityNodeInfo> nodeInfoList = accessibilityNodeInfo.findAccessibilityNodeInfosByText(text);
        if (nodeInfoList != null && !nodeInfoList.isEmpty()) {
            for (AccessibilityNodeInfo nodeInfo : nodeInfoList) {
                if (nodeInfo != null && (text.equals(nodeInfo.getText()) || text.equals(nodeInfo.getContentDescription()))) {
                    performClick(nodeInfo);
                    break;
                }
            }
        }
    }

    /**
     * 查找view
     *
     * @param accessibilityService
     * @param id
     */
    public static boolean findViewId(AccessibilityService accessibilityService, String id) {
        if (accessibilityService == null) {
            return false;
        }
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityService.getRootInActiveWindow();
        if (accessibilityNodeInfo == null) {
            return false;
        }
        List<AccessibilityNodeInfo> nodeInfoList = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId(id);
        return nodeInfoList != null && !nodeInfoList.isEmpty();
    }

    /**
     * 检查viewId进行点击
     *
     * @param accessibilityService
     * @param id
     */
    public static void findViewIdAndClick(AccessibilityService accessibilityService, String id) {
        if (accessibilityService == null) {
            return;
        }
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityService.getRootInActiveWindow();
        if (accessibilityNodeInfo == null) {
            return;
        }

        List<AccessibilityNodeInfo> nodeInfoList = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId(id);
        if (nodeInfoList != null && !nodeInfoList.isEmpty()) {
            for (AccessibilityNodeInfo nodeInfo : nodeInfoList) {
                if (nodeInfo != null) {
                    performClick(nodeInfo);
                    break;
                }
            }
        }
    }

    /**
     * 查找控件，并将焦点放在该空间上
     *
     * @param accessibilityService
     * @param id
     */
    public static void findViewIdAndFouce(AccessibilityService accessibilityService, String id) {
        if (accessibilityService == null) {
            return;
        }
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityService.getRootInActiveWindow();
        if (accessibilityNodeInfo == null) {
            return;
        }

        List<AccessibilityNodeInfo> nodeInfoList = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId(id);
        if (nodeInfoList != null && !nodeInfoList.isEmpty()) {
            for (AccessibilityNodeInfo nodeInfo : nodeInfoList) {
                if (nodeInfo != null) {
                    nodeInfo.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    break;
                }
            }
        }
    }


    /**
     * 根据id查找控件，并将内容粘贴上去
     *
     * @param accessibilityService
     * @param id
     * @param content
     * @return
     */
    public static boolean findViewByIdAndPasteContent(AccessibilityService accessibilityService, String id, String content) {
        if (accessibilityService == null) {
            return false;
        }
        AccessibilityNodeInfo rootNode = accessibilityService.getRootInActiveWindow();
        if (rootNode != null) {
            List<AccessibilityNodeInfo> editInfo = rootNode.findAccessibilityNodeInfosByViewId(id);
            if (editInfo != null && !editInfo.isEmpty()) {
                Bundle arguments = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, content);
                editInfo.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 根据id查找控件上的文字描述
     *
     * @param accessibilityService
     * @param id
     * @return
     */
    public static String findTextById(AccessibilityService accessibilityService, String id) {
        if (accessibilityService != null) {
            AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
            if (rootInfo != null) {
                List<AccessibilityNodeInfo> userNames = rootInfo.findAccessibilityNodeInfosByViewId(id);
                if (userNames != null && userNames.size() > 0) {
                    String name = userNames.get(0).getText().toString();
                    rootInfo.recycle();
                    return name;
                }
                rootInfo.recycle();
            }
        }
        return null;
    }

    /**
     * 根据id查找控件上的文字描述
     */
    public static String findTextById(AccessibilityNodeInfo nodeInfo, String id) {
        AccessibilityNodeInfo rootInfo = nodeInfo;
        if (rootInfo != null) {
            List<AccessibilityNodeInfo> userNames = rootInfo.findAccessibilityNodeInfosByViewId(id);
            if (userNames != null && userNames.size() > 0) {
                String name = userNames.get(0).getText().toString();
                return name;
            }
        }
        return null;
    }


    /**
     * 在当前页面查找对话框文字内容并点击
     *
     * @param text1 默认点击text1
     * @param text2
     */
    public static void findDialogAndClick(AccessibilityService accessibilityService, String text1, String text2) {
        if (accessibilityService == null) {
            return;
        }
        AccessibilityNodeInfo accessibilityNodeInfo = accessibilityService.getRootInActiveWindow();
        if (accessibilityNodeInfo == null) {
            return;
        }

        List<AccessibilityNodeInfo> dialogWait = accessibilityNodeInfo.findAccessibilityNodeInfosByText(text1);
        List<AccessibilityNodeInfo> dialogConfirm = accessibilityNodeInfo.findAccessibilityNodeInfosByText(text2);
        if (!dialogWait.isEmpty() && !dialogConfirm.isEmpty()) {
            for (AccessibilityNodeInfo nodeInfo : dialogWait) {
                if (nodeInfo != null && text1.equals(nodeInfo.getText())) {
                    performClick(nodeInfo);
                    break;
                }
            }
        }
    }

    /**
     * 根据getRootInActiveWindow查找包含当前text的控件
     *
     * @param containsText 只要内容包含就会找到（应该是根据drawText找的）
     */
    @Nullable
    public static List<AccessibilityNodeInfo> findViewByContainsText(String containsText, AccessibilityService accessibilityService) {
        if (accessibilityService == null) {
            return null;
        }
        AccessibilityNodeInfo info = accessibilityService.getRootInActiveWindow();
        if (info == null) return null;
        List<AccessibilityNodeInfo> list = info.findAccessibilityNodeInfosByText(containsText);
        info.recycle();
        return list;
    }


    /**
     * 根据getRootInActiveWindow查找包含当前text的控件
     */
    @Nullable
    public static List<AccessibilityNodeInfo> findViewByContainsText(AccessibilityNodeInfo parent, String className) {
        List<AccessibilityNodeInfo> list = parent.findAccessibilityNodeInfosByText(className);
        parent.recycle();
        return list;
    }

    /**
     * 根据getRootInActiveWindow查找和当前text相等的控件
     *
     * @param equalsText 需要找的text
     */
    @Nullable
    public static List<AccessibilityNodeInfo> findViewByEqualsText(String equalsText, AccessibilityService accessibilityService) {
        List<AccessibilityNodeInfo> listOld = findViewByContainsText(equalsText, accessibilityService);
        if (Utils.isEmptyArray(listOld)) {
            return null;
        }
        ArrayList<AccessibilityNodeInfo> listNew = new ArrayList<>();
        for (AccessibilityNodeInfo ani : listOld) {
            if (ani.getText() != null && equalsText.equals(ani.getText().toString())) {
                listNew.add(ani);
            } else {
                ani.recycle();
            }
        }
        return listNew;
    }

    /**
     * 根据getRootInActiveWindow查找当前id的控件
     *
     * @param pageName 被查找项目的包名:com.android.xxx
     * @param idName   id值:tv_main
     */
    @Nullable
    public static AccessibilityNodeInfo findViewById(String pageName, String idName, AccessibilityService accessibilityService) {
        return findViewById(pageName + ":id/" + idName, accessibilityService);
    }

    /**
     * 根据getRootInActiveWindow查找当前id的控件
     *
     * @param idfullName id全称:com.android.xxx:id/tv_main
     */
    @Nullable
    public static AccessibilityNodeInfo findViewById(String idfullName, AccessibilityService accessibilityService) {
        List<AccessibilityNodeInfo> list = findViewByIdList(idfullName, accessibilityService);
        return Utils.isEmptyArray(list) ? null : list.get(0);
    }

    /**
     * 根据getRootInActiveWindow查找当前id的控件集合(类似listview这种一个页面重复的id很多)
     *
     * @param idfullName id全称:com.android.xxx:id/tv_main
     */
    @Nullable
    public static List<AccessibilityNodeInfo> findViewByIdList(String idfullName, AccessibilityService accessibilityService) {
        try {
            AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
            if (rootInfo == null) return null;
            List<AccessibilityNodeInfo> list = rootInfo.findAccessibilityNodeInfosByViewId(idfullName);
            rootInfo.recycle();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 只找第一个ClassName
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstClassName(String className, AccessibilityService accessibilityService) {
        if (accessibilityService == null) {
            return null;
        }
        AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
        if (rootInfo == null) return null;
        AccessibilityNodeInfo info = findViewByFirstClassName(rootInfo, className);
        rootInfo.recycle();
        return info;
    }

    /**
     * 只找第一个ClassName
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstClassName(AccessibilityNodeInfo parent, String className) {
        if (parent == null) return null;
        for (int i = 0; i < parent.getChildCount(); i++) {
            AccessibilityNodeInfo child = parent.getChild(i);
            if (child == null) continue;
            if (className.equals(child.getClassName().toString())) {
                return child;
            }
            AccessibilityNodeInfo childChild = findViewByFirstClassName(child, className);
            child.recycle();
            if (childChild != null) {
                return childChild;
            }
        }
        return null;
    }

    /**
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static List<AccessibilityNodeInfo> findViewByClassName(String className, AccessibilityService accessibilityService) {
        ArrayList<AccessibilityNodeInfo> list = new ArrayList<>();
        AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
        if (rootInfo == null) return list;
        findViewByClassName(list, rootInfo, className);
        rootInfo.recycle();
        return list;
    }

    /**
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static void findViewByClassName(List<AccessibilityNodeInfo> list, AccessibilityNodeInfo parent, String className) {
        if (parent == null) return;
        for (int i = 0; i < parent.getChildCount(); i++) {
            AccessibilityNodeInfo child = parent.getChild(i);
            if (child == null) continue;
            if (className.equals(child.getClassName().toString())) {
                list.add(child);
            } else {
                findViewByClassName(list, child, className);
                child.recycle();
            }
        }
    }

    /**
     * 只找第一个相等的ContentDescription
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstEqualsContentDescription(String contentDescription, AccessibilityService accessibilityService) {
        if (accessibilityService == null) {
            return null;
        }
        AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
        if (rootInfo == null) return null;
        AccessibilityNodeInfo info = findViewByFirstEqualsContentDescription(rootInfo, contentDescription);
        rootInfo.recycle();
        return info;
    }

    /**
     * 只找第一个相等的ContentDescription
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstEqualsContentDescription(AccessibilityNodeInfo parent, String contentDescription) {
        if (parent == null) return null;
        for (int i = 0; i < parent.getChildCount(); i++) {
            AccessibilityNodeInfo child = parent.getChild(i);
            if (child == null) continue;
            CharSequence cd = child.getContentDescription();
            if (cd != null && contentDescription.equals(cd.toString())) {
                return child;
            }
            AccessibilityNodeInfo childChild = findViewByFirstEqualsContentDescription(child, contentDescription);
            child.recycle();
            if (childChild != null) {
                return childChild;
            }
        }
        return null;
    }

    /**
     * 只找第一个包含的ContentDescription
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstContainsContentDescription(String contentDescription, AccessibilityService accessibilityService) {
        if (accessibilityService != null) {
            AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
            if (rootInfo == null) return null;
            AccessibilityNodeInfo info = findViewByFirstContainsContentDescription(rootInfo, contentDescription);
            rootInfo.recycle();
            return info;
        }
        return null;
    }

    /**
     * 只找第一个包含的ContentDescription
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static AccessibilityNodeInfo findViewByFirstContainsContentDescription(AccessibilityNodeInfo parent, String contentDescription) {
        if (parent == null) return null;
        for (int i = 0; i < parent.getChildCount(); i++) {
            AccessibilityNodeInfo child = parent.getChild(i);
            if (child == null) continue;
            CharSequence cd = child.getContentDescription();
            if (cd != null && cd.toString().contains(contentDescription)) {
                return child;
            }
            AccessibilityNodeInfo childChild = findViewByFirstContainsContentDescription(child, contentDescription);
            child.recycle();
            if (childChild != null) {
                return childChild;
            }
        }
        return null;
    }

    /**
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static List<AccessibilityNodeInfo> findViewByContentDescription(String contentDescription, AccessibilityService accessibilityService) {
        if (accessibilityService == null) {
            return null;
        }
        ArrayList<AccessibilityNodeInfo> list = new ArrayList<>();
        AccessibilityNodeInfo rootInfo = accessibilityService.getRootInActiveWindow();
        if (rootInfo == null) return null;
        findViewByContentDescription(list, rootInfo, contentDescription);
        rootInfo.recycle();
        return list;
    }

    /**
     * 此方法效率相对较低,建议使用之后保存id然后根据id进行查找
     */
    public static void findViewByContentDescription(List<AccessibilityNodeInfo> list, AccessibilityNodeInfo parent, String contentDescription) {
        if (parent == null) return;
        for (int i = 0; i < parent.getChildCount(); i++) {
            AccessibilityNodeInfo child = parent.getChild(i);
            if (child == null) continue;
            CharSequence cd = child.getContentDescription();
            if (cd != null && contentDescription.equals(cd.toString())) {
                list.add(child);
            } else {
                findViewByContentDescription(list, child, contentDescription);
                child.recycle();
            }
        }
    }


    /**
     * 由于太多,最好回收这些AccessibilityNodeInfo
     */
    public static void recycleAccessibilityNodeInfo(List<AccessibilityNodeInfo> listInfo) {
        if (Utils.isEmptyArray(listInfo)) return;

        for (AccessibilityNodeInfo info : listInfo) {
            info.recycle();
        }
    }


    /**
     * 将内容粘贴到对应的控件上
     *
     * @param accessibilityService
     * @param nodeInfo
     * @param content
     */
    public static void pastContent(AccessibilityService accessibilityService, AccessibilityNodeInfo nodeInfo, String content) {
        Bundle arguments = new Bundle();
        arguments.putInt(AccessibilityNodeInfo.ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT,
                AccessibilityNodeInfo.MOVEMENT_GRANULARITY_WORD);
        arguments.putBoolean(AccessibilityNodeInfo.ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN, true);
        nodeInfo.performAction(AccessibilityNodeInfo.ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY, arguments);
        nodeInfo.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
        ClipData clip = ClipData.newPlainText("label", content);
        ClipboardManager clipboardManager = (ClipboardManager) accessibilityService.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(clip);
        nodeInfo.performAction(AccessibilityNodeInfo.ACTION_PASTE);
    }

    //模拟点击事件
    public static void performClick(AccessibilityNodeInfo nodeInfo) {
        if (nodeInfo == null) {
            return;
        }
        if (nodeInfo.isClickable()) {
            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_CLICK);
        } else {
            performClick(nodeInfo.getParent());
        }
    }

    //模拟长点击事件
    public static void performLongClick(AccessibilityNodeInfo nodeInfo) {
        if (nodeInfo == null) {
            return;
        }
        if (nodeInfo.isLongClickable()) {
            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
        } else {
            performLongClick(nodeInfo.getParent());
        }
    }

    //模拟向下滚事件
    public static void performScrollForWard(AccessibilityNodeInfo nodeInfo) {
        if (nodeInfo == null) {
            return;
        }
        if (nodeInfo.isScrollable()) {
            nodeInfo.performAction(AccessibilityNodeInfo.ACTION_SCROLL_FORWARD);
        } else {
            performScrollForWard(nodeInfo.getParent());
        }
    }


    //模拟返回事件
    public static void performBack(AccessibilityService service) {
        if (service == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            service.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
        }
    }

    //获取按钮id
    public static String getNodeId(String id) {
        return "com.tencent.mm:id/" + id;
    }
}
